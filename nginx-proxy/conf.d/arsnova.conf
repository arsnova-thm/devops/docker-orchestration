upstream core {
  server arsnova-backend-core:8080;
}

upstream core-ws {
  server arsnova-backend-core:8090;
}

upstream comment {
  server arsnova-comment-service:8080;
}

upstream ws-gateway {
  server arsnova-ws-gateway:8080;
}

server {
  listen 80 default_server;
  listen [::]:80 default_server;
  server_name localhost _;

  # Modern client
  location / {
    proxy_pass http://arsnova-web;
    proxy_http_version 1.1;
    gzip on;
    gzip_types application/javascript text/css image/svg+xml;
    gzip_vary on;
  }

  # Legacy client
  location /mobile/ {
    proxy_pass http://arsnova-web-legacy/;
    proxy_http_version 1.1;
    gzip on;
    gzip_types application/javascript text/css image/svg+xml;
    gzip_vary on;
  }

  # Core REST API
  location /api/ {
    proxy_pass http://core/;
    proxy_http_version 1.1;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $forwarded_scheme;
    proxy_set_header X-Forwarded-Port $forwarded_port;
  }

  # STOMP WebSocket server
  location /api/ws/ {
    proxy_pass http://ws-gateway/ws/;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $forwarded_scheme;
    proxy_set_header X-Forwarded-Port $forwarded_port;
  }

  # Comment Service REST API
  location ~ /api/(bonustoken|comment|settings|vote)(.*) {
    proxy_pass http://comment/$1$2;
    proxy_http_version 1.1;
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $forwarded_scheme;
    proxy_set_header X-Forwarded-Port $forwarded_port;
  }

  # Legacy WebSocket server
  location /socket.io/ {
    proxy_pass http://core-ws;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_set_header Host $host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $forwarded_scheme;
    proxy_set_header X-Forwarded-Port $forwarded_port;
  }

  # Legacy configuration endpoint
  location = /arsnova-config { proxy_pass http://core/v2/configuration/; }
}

# Set server's scheme as fallback if request was not already forwarded.
map $http_x_forwarded_for $forwarded_scheme_fallback {
  default '';
  ''      $scheme;
}

# Use existing header or fallback.
map $http_x_forwarded_proto $forwarded_scheme {
  default $http_x_forwarded_proto;
  ''      $forwarded_scheme_fallback;
}

# Extract port from request's Host header
map $http_host $request_port {
  default     '';
  ~:([0-9]+)$ $1;
}

# Set request's port as fallback if request was not already forwarded.
map $http_x_forwarded_for $forwarded_port_fallback {
  default '';
  ''      $request_port;
}

# Use existing header or fallback.
map $http_x_forwarded_port $forwarded_port {
  default $http_x_forwarded_port;
  ''      $forwarded_port_fallback;
}
